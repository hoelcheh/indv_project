require 'test_helper'

class PhysicianAvailabilitiesControllerTest < ActionController::TestCase
  setup do
    @physician_availability = physician_availabilities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:physician_availabilities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create physician_availability" do
    assert_difference('PhysicianAvailability.count') do
      post :create, physician_availability: @physician_availability.attributes
    end

    assert_redirected_to physician_availability_path(assigns(:physician_availability))
  end

  test "should show physician_availability" do
    get :show, id: @physician_availability
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @physician_availability
    assert_response :success
  end

  test "should update physician_availability" do
    put :update, id: @physician_availability, physician_availability: @physician_availability.attributes
    assert_redirected_to physician_availability_path(assigns(:physician_availability))
  end

  test "should destroy physician_availability" do
    assert_difference('PhysicianAvailability.count', -1) do
      delete :destroy, id: @physician_availability
    end

    assert_redirected_to physician_availabilities_path
  end
end
