require 'test_helper'

class WeekdayAvailabilitiesControllerTest < ActionController::TestCase
  setup do
    @weekday_availability = weekday_availabilities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:weekday_availabilities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create weekday_availability" do
    assert_difference('WeekdayAvailability.count') do
      post :create, weekday_availability: @weekday_availability.attributes
    end

    assert_redirected_to weekday_availability_path(assigns(:weekday_availability))
  end

  test "should show weekday_availability" do
    get :show, id: @weekday_availability
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @weekday_availability
    assert_response :success
  end

  test "should update weekday_availability" do
    put :update, id: @weekday_availability, weekday_availability: @weekday_availability.attributes
    assert_redirected_to weekday_availability_path(assigns(:weekday_availability))
  end

  test "should destroy weekday_availability" do
    assert_difference('WeekdayAvailability.count', -1) do
      delete :destroy, id: @weekday_availability
    end

    assert_redirected_to weekday_availabilities_path
  end
end
