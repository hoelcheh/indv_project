class Appointment < ActiveRecord::Base

  belongs_to :physician
  belongs_to :patient
  has_many :diagnostic_codes
  has_many :weekday_availabilities

end
