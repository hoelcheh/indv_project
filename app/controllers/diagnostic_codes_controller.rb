class DiagnosticCodesController < ApplicationController
  # GET /diagnostic_codes
  # GET /diagnostic_codes.json
  def index
    @diagnostic_codes = DiagnosticCode.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @diagnostic_codes }
    end
  end

  # GET /diagnostic_codes/1
  # GET /diagnostic_codes/1.json
  def show
    @diagnostic_code = DiagnosticCode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @diagnostic_code }
    end
  end

  # GET /diagnostic_codes/new
  # GET /diagnostic_codes/new.json
  def new
    @diagnostic_code = DiagnosticCode.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @diagnostic_code }
    end
  end

  # GET /diagnostic_codes/1/edit
  def edit
    @diagnostic_code = DiagnosticCode.find(params[:id])
  end

  # POST /diagnostic_codes
  # POST /diagnostic_codes.json
  def create
    @diagnostic_code = DiagnosticCode.new(params[:diagnostic_code])

    respond_to do |format|
      if @diagnostic_code.save
        format.html { redirect_to @diagnostic_code, notice: 'Diagnostic code was successfully created.' }
        format.json { render json: @diagnostic_code, status: :created, location: @diagnostic_code }
      else
        format.html { render action: "new" }
        format.json { render json: @diagnostic_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /diagnostic_codes/1
  # PUT /diagnostic_codes/1.json
  def update
    @diagnostic_code = DiagnosticCode.find(params[:id])

    respond_to do |format|
      if @diagnostic_code.update_attributes(params[:diagnostic_code])
        format.html { redirect_to @diagnostic_code, notice: 'Diagnostic code was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @diagnostic_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diagnostic_codes/1
  # DELETE /diagnostic_codes/1.json
  def destroy
    @diagnostic_code = DiagnosticCode.find(params[:id])
    @diagnostic_code.destroy

    respond_to do |format|
      format.html { redirect_to diagnostic_codes_url }
      format.json { head :no_content }
    end
  end
end
