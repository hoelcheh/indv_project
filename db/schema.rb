# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141201202535) do

  create_table "appointments", :force => true do |t|
    t.integer  "patient_id"
    t.integer  "physician_id"
    t.integer  "diagnostic_code_id"
    t.integer  "weekday_availability_id"
    t.text     "reason_for_visit"
    t.text     "physician_note"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "diagnostic_codes", :force => true do |t|
    t.float  "code_number"
    t.string   "code_name"
    t.float  "cost"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "patients", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "phone_num"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "physician_availabilities", :force => true do |t|
    t.string   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "physicians", :force => true do |t|
    t.string   "last_name"
    t.integer  "physician_availability_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "weekday_availabilities", :force => true do |t|
    t.string   "w_day"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
