class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.integer :phone_num
      t.string :email

      t.timestamps
    end
  end
end
