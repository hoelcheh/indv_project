class CreatePhyAvailabilities < ActiveRecord::Migration
  def change
    create_table :phy_availabilities do |t|
      t.string :status

      t.timestamps
    end
  end
end
