class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :last_name
      t.integer :physician_availability_id

      t.timestamps
    end
  end
end
