class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.integer :diagnostic_code_id
      t.integer :weekday_availability_id
      t.text :reason_for_visit
      t.text :physician_note

      t.timestamps
    end
  end
end
